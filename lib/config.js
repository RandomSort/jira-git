var Promise = require("bluebird");
var chalk = require("chalk");
var fs = require("fs");
var prompt = require("inquirer").createPromptModule();
var _ = require("lodash");
var request = require("superagent");

var program = require("./program");
var git = require("./git");
var handleError = require("./handleError");

function getConfig() {

    var config = {};

    return Promise.coroutine(function* () {

        var gitPath = undefined; // make IDEA happy
        yield git(["rev-parse", "--git-dir"], {
            onLine: function(line) {
                gitPath = line.trim();
            }
        });
        if (gitPath === undefined) {
            throw new Error("This command must be run within a Git repository.");
        }
        var configPath = gitPath + "/.jira";

        if (!program.configure) {
            try {
                config = JSON.parse(fs.readFileSync(configPath, "utf8"));
                if (config.password) {
                    config.password = new Buffer(config.password, "base64").toString("utf8");
                }
            } catch (err) {
                program.debug && console.error(chalk.red("Couldn't read from " + configPath));
            }
        }

        var questions = [];

        if (!(config.url || config.apiUrl)) {
            questions.push({
                type: "input",
                name: "url",
                message: "JIRA URL (e.g. https://jira.atlassian.com/):",
                validate: function(url) {
                    if (url && url.indexOf("http") === 0) {
                        return true;
                    }
                    return "Please enter a URL starting with http(s)://"
                },
                filter: function(url) {
                    url = url.trim();
                    url = url.charAt(url.length - 1) === "/" ? url : url + "/";
                    return url;
                }
            });
        }

        if (!(config.username || config.password)) {
            questions.push({
                type: "input",
                name: "username",
                message: "Username:",
                validate: function(username) {
                    return (username && username.trim().length > 0) || "Please enter your JIRA username"
                },
                filter: function(username) {
                    return username.trim();
                }
            });
            questions.push({
                type: "password",
                name: "password",
                message: "Password:",
                validate: function(password) {
                    return (password && password.trim().length > 0) || "Please enter your JIRA password"
                },
                filter: function(password) {
                    return password.trim();
                }
            });
        }

        if (questions.length > 0) {
            console.log("Config will be stored in " + configPath);
            config = _.extend(config, yield prompt(questions));

            // test connection
            var res = yield request
                .get(config.url + "rest/gadget/1.0/currentUser")
                .auth(config.username, config.password)
                .set('Accept', 'application/json');

            if (!res.ok) {
                return handleError(res);
            }

            console.log("Authenticated as " + res.body.fullName);

            var encodedConfig = _.extend({}, config, {
                password: new Buffer(config.password).toString("base64")
            });
            fs.writeFileSync(configPath, JSON.stringify(encodedConfig, null, "  "), {
                encoding: "utf8",
                mode: 0o600 // read-only to the current user
            });
        }

        // properties below here will not be automatically persisted

        config.apiUrl = config.apiUrl || config.url + "rest/api/2/";
        config.branchNameFormat = config.branchNameFormat || "feature/%KEY%-%SUMMARY%";
        config.branchNameMaxSummaryLength = config.branchNameMaxSummaryLength || 50;
        config.maxSummaryDisplayLength = config.maxSummaryDisplayLength || 72;

        return config;
    })().catch(function(err) {
        return handleError(err);
    });
}

module.exports = getConfig;
