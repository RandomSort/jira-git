#!/usr/bin/env node

var Promise = require("bluebird");
var chalk = require("chalk");
var program = require("./lib/program");
var jira = require("./lib/jira");
var getConfig = require("./lib/config");

var issueKeys = program.args;

Promise.coroutine(function* () {
    if (program.configure) {
        yield getConfig();
    } else if (program.jql) {
        if (program.branch) {
            console.log(chalk.red("The -b/--branch and -j/--jql options can not be combined."));
            process.exit(1);
        }
        var jql = program.args.join(" ");
        yield jira.executeJql(jql);
    } else {
        if (program.branch && issueKeys.length !== 1) {
            console.log(chalk.red("You must specify exactly one issue key when using the -b/--branch option."));
            process.exit(1);
        }
        if (!issueKeys.length) {
            issueKeys.push(yield jira.getContextIssueKey());
        }
        for (var i = 0; i < issueKeys.length; i++) {
            yield jira.displayIssue(issueKeys[i]);
        }
    }
})();

